﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv2 : MonoBehaviour {
	public float speed;
	public float runSpeed;
	private float hInput, vInput;
	public bool hrotacion;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		hInput = Input.GetAxisRaw ("Horizontal");
		vInput = Input.GetAxisRaw ("Vertical");

	

		float step = speed;

		if (Input.GetButton("Run")){
			step = runSpeed;
		}
		if (vInput != 0) {
			transform.Translate (vInput * Vector3.forward * Time.deltaTime * step);
		}
		if (hInput != 0) {
			if (!hrotacion) {
				transform.Translate (hInput * Vector3.right * Time.deltaTime * step * 100);
			}
			if (hrotacion) {
				transform.Rotate (hInput * Vector3.right * Time.deltaTime * step * 100);
			}
		}
	}
}
