﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambiarColor : MonoBehaviour {
	public bool changeColor;
	public Color aleatorio;
	public Renderer rend;
	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		 
		if (Input.GetButton ("Color")) {
			aleatorio = new Color32 ((byte)Random.Range (0, 255),(byte) Random.Range (0, 255), (byte)Random.Range (0, 255), 255);
			rend.material.SetColor ("_Color", aleatorio);
		} else {
			rend.material.SetColor ("_Color", Color.white);
		}

		if (!changeColor) {
			rend.material.SetColor ("_Color", Color.red);
		}
	}
}
