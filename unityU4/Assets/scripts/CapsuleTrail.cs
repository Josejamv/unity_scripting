﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleTrail : MonoBehaviour {
	private TrailRenderer trail;

	[Range(1, 50)]
	public float startWidht = 5;
	[Range(1, 50)]
	public float endWidht = 2;

	private void Start(){
		if (!(trail = GetComponent<TrailRenderer> ())) {

			Debug.LogError ("No TrailRenderer Component attached to the GameObject.");
			enabled = false;
			return;
		}
	
	
	
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<TrailRenderer> ().time = Random.Range (1, 100);
		GetComponent<TrailRenderer> ().startWidth = startWidht;
		GetComponent<TrailRenderer> ().endWidth = endWidht;

	}
}
