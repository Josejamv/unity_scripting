﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escala : MonoBehaviour {
	Vector3 escalaInicial;

	// Use this for initialization
	void Start () {
		escalaInicial = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Escala")) {
			transform.localScale += new Vector3 (Random.Range(0, 0.5f),Random.Range(0, 0.5f), Random.Range(0, 0.5f));
		} else {
			transform.localScale = escalaInicial;
		}

	}
}
