﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour {
	public float speed = 5f;

	// Use this for initialization
	void Start () {
		transform.Translate (new Vector3 (speed * Time.deltaTime, 0, 0));
		Debug.Log ("Moving From Start Event");
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3 (0, 0, -speed * Time.deltaTime));
		Debug.Log ("Moving From Update Event");
		
	}
}
