﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{

    private Transform cameraTransform;
    private Camera cam;
    void Start()
    {
        cam = gameObject.GetComponent<Camera>();
        cameraTransform = gameObject.transform;
        
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 10000))
            {
                Debug.DrawLine(ray.origin, hit.point);
                Destroy(hit.transform.gameObject);
            }
        }
    }
}