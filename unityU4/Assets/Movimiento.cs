﻿using UnityEngine;
using System.Collections;

public class Movimiento: MonoBehaviour
{

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;
    private Rigidbody rb;
    public float hInput, vInput;
    public float velocidad;


    void Update()
    {
        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);

        hInput = Input.GetAxisRaw("Horizontal");
        vInput = Input.GetAxisRaw("Vertical");
        rb = gameObject.GetComponent<Rigidbody>();
        if (Input.GetButton("Horizontal"))
        {
            rb.velocity = (new Vector3(0, 0, hInput) * velocidad);
        }
        if (Input.GetButton("Vertical")) {
            rb.velocity = (new Vector3(vInput, 0, 0) * velocidad);
        }
    }

}
