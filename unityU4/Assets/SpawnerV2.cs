﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnerV2 : MonoBehaviour
{

    public GameObject objectToSpawn;
    [Range(0.5f, 3)]
    public float instanciacion;
    private float contador;
    private Renderer color;
    public Color newcolor;
    private Vector3[] V = { Vector3.up, Vector3.down, Vector3.right, Vector3.left };

    void Start()
    {


        color = objectToSpawn.GetComponent<Renderer>();
        color.sharedMaterial.SetColor("_Color", newcolor);

    }
    void Update()
    {

        if (contador > instanciacion)
        {
            GameObject objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;



            float escala = Random.Range(1, 10);

            objAux.transform.localScale = new Vector3(escala, escala, escala);

            Rigidbody rb = objAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 25);

            rb.AddForce(V[Random.Range(0, 3)] * Random.Range(10, 150), ForceMode.Impulse);
            contador = 0;
        }
        contador += Time.deltaTime;
    }
}
